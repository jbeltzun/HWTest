//
//  City.h
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (nonatomic, readonly) NSString *idCity;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *country;
@property (nonatomic, readonly) NSString *idCountry;


- (instancetype)initWithDictionary:(NSDictionary *)dictionary;


@end
