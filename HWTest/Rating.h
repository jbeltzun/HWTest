//
//  City.h
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rating : NSObject

@property (nonatomic, readonly) NSInteger overall;
@property (nonatomic, readonly) NSInteger numberOfRatings;


- (instancetype)initWithDictionary:(NSDictionary *)dictionary;


@end
