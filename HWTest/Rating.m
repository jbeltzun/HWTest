//
//  City.m
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import "Rating.h"

@implementation Rating

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (dictionary == nil || !(self = [super init])) {
        return nil;
    }
    if (!([dictionary[@"overall"] isEqual:[NSNull null]])) {
        _overall = [dictionary[@"overall"] integerValue];
    }
    if (!([dictionary[@"numberOfRatings"] isEqual:[NSNull null]])) {
        _numberOfRatings = [dictionary[@"numberOfRatings"] integerValue];
    }
 
    return self;
}

@end
