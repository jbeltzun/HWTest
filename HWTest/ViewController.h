//
//  ViewController.h
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (NSArray *)JSONToArrayTransformer:(NSArray *)arrayOfDictionaries transformerBlock:(id (^)(NSDictionary *dictionary))transformerBlock;

@end

