//
//  ViewController.m
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import "ViewController.h"
#import "Property.h"
#import "PropertyCell.h"
#import "DetailViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property NSMutableArray *propertiesArray;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.propertiesArray = [NSMutableArray new];

    NSURL *url = [NSURL URLWithString:@"http://private-anon-bebfdf58cf-practical3.apiary-mock.com/cities/1530/properties/"];

    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {

                
                id jsonData =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                
                NSArray *properties = [self JSONToArrayTransformer:jsonData[@"properties"] transformerBlock:^id(NSDictionary *dictionary) {
                    Property *prop = [[Property alloc] initWithDictionary:dictionary];
                    return prop;
                }];
                NSLog(@"Found %ld properties!", properties.count);
                [self.propertiesArray addObjectsFromArray:properties];
               dispatch_async(dispatch_get_main_queue(), ^{
                   [self.tableView reloadData];
               });
                
            }] resume];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.propertiesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Property *prop =  self.propertiesArray[indexPath.row];
    PropertyCell *cell = (PropertyCell*)[tableView dequeueReusableCellWithIdentifier:@"PropertyCell" forIndexPath:indexPath];
  
    cell.propertyObject = prop;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    Property *prop =  self.propertiesArray[indexPath.row];
    DetailViewController *detailView =[mainStoryboard instantiateViewControllerWithIdentifier:
                                       @"DetailViewController"];
    detailView.property = prop;
    [self presentViewController:detailView animated:YES completion:nil];
}



- (NSArray *)JSONToArrayTransformer:(NSArray *)arrayOfDictionaries transformerBlock:(id (^)(NSDictionary *dictionary))transformerBlock {
    if (![arrayOfDictionaries isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray array];
    for (NSDictionary *dictionary in arrayOfDictionaries) {
        if (![dictionary isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        id object = transformerBlock(dictionary);
        if (!object) {
            continue;
        }
        [result addObject:object];
    }
    return result.count > 0 ? result : nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
