//
//  City.m
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import "Property.h"

@implementation Property

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (dictionary == nil || !(self = [super init])) {
        return nil;
    }

    _idProperty = dictionary[@"id"];
    _name = dictionary[@"name"];
    _city = [[City alloc] initWithDictionary:dictionary[@"city"]];
    _type = dictionary[@"type"];
    _rating = [[Rating alloc] initWithDictionary:dictionary[@"overallRating"]];
    
    _directions = dictionary[@"directions"];
    _propertyDescription = dictionary[@"description"];
    _address1 = dictionary[@"address1"];
    _address2 = dictionary[@"address2"];

    
    NSMutableArray *tmpImages = [NSMutableArray new];
    for (NSDictionary *im in dictionary[@"images"]) {
        NSString *url = [NSString stringWithFormat:@"%@%@",im[@"prefix"], im[@"suffix"]];
        [tmpImages addObject: url];
    }
    _images = [[NSArray alloc] initWithArray:tmpImages];
    return self;
}

@end
