//
//  PropertyCell.h
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface PropertyCell : UITableViewCell

@property (nonatomic) Property *propertyObject;

- (void)setPropertyObject:(Property *)property;
@end
