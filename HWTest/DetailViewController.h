//
//  DetailViewController.h
//  HWTest
//
//  Created by Julien Beltzung on 20/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Property.h"

@interface DetailViewController : UIViewController

@property (nonatomic) Property *property;

- (void)setProperty:(Property *)property;

@end
