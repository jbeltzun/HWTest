//
//  PropertyCell.m
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import "PropertyCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PropertyCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLAbel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;

@end


@implementation PropertyCell


- (void)setPropertyObject:(Property *)property {
    
    _propertyObject = property;
    self.nameLAbel.text = property.name;
    self.typeLabel.text = property.type;
    if (property.rating.overall) {
        self.ratingLabel.text = [NSString stringWithFormat:@"%ld/100", property.rating.overall];
    } else {
        self.ratingLabel.text = @"No rating";
    }
    if (property.images.count > 0) {
        [self.thumbnail sd_setImageWithURL:[NSURL URLWithString:property.images.firstObject]];
    }
}



@end
