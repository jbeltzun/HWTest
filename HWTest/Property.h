//
//  City.h
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "City.h"
#import "Rating.h"

@interface Property : NSObject

@property (nonatomic, readonly) NSString *idProperty;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) City *city;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) Rating *rating;
@property (nonatomic, readonly) NSArray *images;

@property (nonatomic, readonly) NSString *address1;
@property (nonatomic, readonly) NSString *address2;
@property (nonatomic, readonly) NSString *propertyDescription;
@property (nonatomic, readonly) NSString *directions;


- (instancetype)initWithDictionary:(NSDictionary *)dictionary;


@end
