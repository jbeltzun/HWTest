//
//  City.m
//  HWTest
//
//  Created by Julien Beltzung on 19/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import "City.h"

@implementation City

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (dictionary == nil || !(self = [super init])) {
        return nil;
    }

    _idCity = dictionary[@"id"];
    _name = dictionary[@"name"];
    _country = dictionary[@"country"];
    _idCountry = dictionary[@"idCountry"];

    return self;
}

@end
