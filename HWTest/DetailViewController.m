//
//  DetailViewController.m
//  HWTest
//
//  Created by Julien Beltzung on 20/10/2016.
//  Copyright © 2016 Julien Beltzung. All rights reserved.
//

#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UILabel *address1;
@property (weak, nonatomic) IBOutlet UILabel *address2;
@property (weak, nonatomic) IBOutlet UILabel *directions;
@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)setProperty:(Property *)property {
    [self view];
    _property = property;
    
    self.name.text = property.name;
    
    if (property.images.count > 0) {
        [self.thumbnail sd_setImageWithURL:[NSURL URLWithString:property.images.firstObject]];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://private-anon-bebfdf58cf-practical3.apiary-mock.com/properties/%@", property.idProperty]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:url
            completionHandler:^(NSData *data,
                                NSURLResponse *response,
                                NSError *error) {
                
                
                id jsonData =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                
                
                Property *fullprop = [[Property alloc] initWithDictionary:jsonData];


                dispatch_async(dispatch_get_main_queue(), ^{
                    self.descriptionTextView.text = fullprop.propertyDescription;
                    self.address1.text = fullprop.address1;
                    self.address2.text = fullprop.address2;
                    self.directions.text = fullprop.directions;
                });
                
            }] resume];
    
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
